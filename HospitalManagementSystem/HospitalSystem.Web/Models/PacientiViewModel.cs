﻿using HospitalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HospitalSystem.Web.Models
{
    public class PacientiViewModel
    {
        public Kontrolla kontrolla { get; set; }
        public Prescription prescription { get; set; }
    }
}