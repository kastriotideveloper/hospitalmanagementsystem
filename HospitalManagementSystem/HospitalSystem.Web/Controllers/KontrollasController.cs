﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HospitalDB;
using HospitalSystem.Web.Models;

namespace HospitalSystem.Web.Controllers
{
    public class KontrollasController : Controller
    {
        private HospitalSystemEntities1 db = new HospitalSystemEntities1();

        // GET: Kontrollas
        public ActionResult Index()
        {
            var kontrollas = db.Kontrollas.Include(k => k.Doctor);
            return View(kontrollas.ToList());
        }

        // GET: Kontrollas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kontrolla kontrolla = db.Kontrollas.Find(id);
            if (kontrolla == null)
            {
                return HttpNotFound();
            }
            return View(kontrolla);
        }

        // GET: Kontrollas/Create
        public ActionResult Create()
        {
            ViewBag.DoctorId = new SelectList(db.Doctors, "Id", "Name");
            return View();
        }

        // POST: Kontrollas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Symptom,Diagnostik,DoctorId")] Kontrolla kontrolla)
        {
            if (ModelState.IsValid)
            {
                db.Kontrollas.Add(kontrolla);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DoctorId = new SelectList(db.Doctors, "Id", "Name", kontrolla.DoctorId);
            return View(kontrolla);
        }

        // GET: Kontrollas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kontrolla kontrolla = db.Kontrollas.Find(id);
            if (kontrolla == null)
            {
                return HttpNotFound();
            }
            ViewBag.DoctorId = new SelectList(db.Doctors, "Id", "Name", kontrolla.DoctorId);
            return View(kontrolla);
        }

        // POST: Kontrollas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Symptom,Diagnostik,DoctorId")] Kontrolla kontrolla)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kontrolla).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DoctorId = new SelectList(db.Doctors, "Id", "Name", kontrolla.DoctorId);
            return View(kontrolla);
        }

        // GET: Kontrollas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kontrolla kontrolla = db.Kontrollas.Find(id);
            if (kontrolla == null)
            {
                return HttpNotFound();
            }
            return View(kontrolla);
        }

        // POST: Kontrollas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kontrolla kontrolla = db.Kontrollas.Find(id);
            db.Kontrollas.Remove(kontrolla);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
