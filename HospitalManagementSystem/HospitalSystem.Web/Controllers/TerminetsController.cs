﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HospitalDB;

namespace HospitalSystem.Web.Controllers
{
    public class TerminetsController : Controller
    {
        private HospitalSystemEntities1 db = new HospitalSystemEntities1();

        // GET: Terminets
        public ActionResult Index()
        {
            var terminets = db.Terminets.Include(t => t.Doctor).Include(t => t.Pacientet);
            return View(terminets.ToList());
        }

        // GET: Terminets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Terminet terminet = db.Terminets.Find(id);
            if (terminet == null)
            {
                return HttpNotFound();
            }
            return View(terminet);
        }

        // GET: Terminets/Create
        public ActionResult Create()
        {
            ViewBag.DoctorId = new SelectList(db.Doctors, "Id", "Name");
            ViewBag.PacientId = new SelectList(db.Pacientets, "Id", "Name");
            return View();
        }

        // POST: Terminets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PacientId,DoctorId,Date")] Terminet terminet)
        {
            if (ModelState.IsValid)
            {
                db.Terminets.Add(terminet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DoctorId = new SelectList(db.Doctors, "Id", "Name", terminet.DoctorId);
            ViewBag.PacientId = new SelectList(db.Pacientets, "Id", "Name", terminet.PacientId);
            return View(terminet);
        }

        // GET: Terminets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Terminet terminet = db.Terminets.Find(id);
            if (terminet == null)
            {
                return HttpNotFound();
            }
            ViewBag.DoctorId = new SelectList(db.Doctors, "Id", "Name", terminet.DoctorId);
            ViewBag.PacientId = new SelectList(db.Pacientets, "Id", "Name", terminet.PacientId);
            return View(terminet);
        }

        // POST: Terminets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PacientId,DoctorId,Date")] Terminet terminet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(terminet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DoctorId = new SelectList(db.Doctors, "Id", "Name", terminet.DoctorId);
            ViewBag.PacientId = new SelectList(db.Pacientets, "Id", "Name", terminet.PacientId);
            return View(terminet);
        }

        // GET: Terminets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Terminet terminet = db.Terminets.Find(id);
            if (terminet == null)
            {
                return HttpNotFound();
            }
            return View(terminet);
        }

        // POST: Terminets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Terminet terminet = db.Terminets.Find(id);
            db.Terminets.Remove(terminet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
