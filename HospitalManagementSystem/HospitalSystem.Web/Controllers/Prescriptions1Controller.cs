﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HospitalDB;

namespace HospitalSystem.Web.Controllers
{
    public class Prescriptions1Controller : Controller
    {
        private HospitalSystemEntities1 db = new HospitalSystemEntities1();

        // GET: Prescriptions1
        public ActionResult Index()
        {
            var prescriptions = db.Prescriptions.Include(p => p.Pacientet);
            return View(prescriptions.ToList());
        }

        // GET: Prescriptions1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prescription prescription = db.Prescriptions.Find(id);
            if (prescription == null)
            {
                return HttpNotFound();
            }
            return View(prescription);
        }

        // GET: Prescriptions1/Create
        public ActionResult Create()
        {
            ViewBag.PaientId = new SelectList(db.Pacientets, "Id", "Name");
            return View();
        }

        // POST: Prescriptions1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,NumOfDays,TakeIt,BeforeMeal,MedicineId,PaientId")] Prescription prescription)
        {
            if (ModelState.IsValid)
            {
                db.Prescriptions.Add(prescription);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PaientId = new SelectList(db.Pacientets, "Id", "Name", prescription.PaientId);
            ViewBag.MedicineId = new SelectList(db.Medicines, "Id", "MedicineName", prescription.MedicineId);
            return View(prescription);
        }

        // GET: Prescriptions1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prescription prescription = db.Prescriptions.Find(id);
            if (prescription == null)
            {
                return HttpNotFound();
            }
            ViewBag.PaientId = new SelectList(db.Pacientets, "Id", "Name", prescription.PaientId);
            ViewBag.MedicineId = new SelectList(db.Medicines, "Id", "MedicineName", prescription.MedicineId);
            return View(prescription);
        }

        // POST: Prescriptions1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,NumOfDays,TakeIt,BeforeMeal,MedicineId,PaientId")] Prescription prescription)
        {
            if (ModelState.IsValid)
            {
                db.Entry(prescription).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PaientId = new SelectList(db.Pacientets, "Id", "Name", prescription.PaientId);
            return View(prescription);
        }

        // GET: Prescriptions1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prescription prescription = db.Prescriptions.Find(id);
            if (prescription == null)
            {
                return HttpNotFound();
            }
            return View(prescription);
        }

        // POST: Prescriptions1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Prescription prescription = db.Prescriptions.Find(id);
            db.Prescriptions.Remove(prescription);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
