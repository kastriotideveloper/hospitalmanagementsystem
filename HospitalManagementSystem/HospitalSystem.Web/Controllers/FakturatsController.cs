﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HospitalDB;

namespace HospitalSystem.Web.Controllers
{
    public class FakturatsController : Controller
    {
        private HospitalSystemEntities1 db = new HospitalSystemEntities1();

        // GET: Fakturats
        public ActionResult Index()
        {
            var fakturats = db.Fakturats.Include(f => f.Pacientet);
            return View(fakturats.ToList());
        }

        // GET: Fakturats/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fakturat fakturat = db.Fakturats.Find(id);
            if (fakturat == null)
            {
                return HttpNotFound();
            }
            return View(fakturat);
        }

        // GET: Fakturats/Create
        public ActionResult Create()
        {
            ViewBag.PacientId = new SelectList(db.Pacientets, "Id", "Name");
            return View();
        }

        // POST: Fakturats/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Int,PacientId,Diagnoza,Qmimi")] Fakturat fakturat)
        {
            if (ModelState.IsValid)
            {
                db.Fakturats.Add(fakturat);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PacientId = new SelectList(db.Pacientets, "Id", "Name", fakturat.PacientId);
            return View(fakturat);
        }

        // GET: Fakturats/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fakturat fakturat = db.Fakturats.Find(id);
            if (fakturat == null)
            {
                return HttpNotFound();
            }
            ViewBag.PacientId = new SelectList(db.Pacientets, "Id", "Name", fakturat.PacientId);
            return View(fakturat);
        }

        // POST: Fakturats/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Int,PacientId,Diagnoza,Qmimi")] Fakturat fakturat)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fakturat).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PacientId = new SelectList(db.Pacientets, "Id", "Name", fakturat.PacientId);
            return View(fakturat);
        }

        // GET: Fakturats/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fakturat fakturat = db.Fakturats.Find(id);
            if (fakturat == null)
            {
                return HttpNotFound();
            }
            return View(fakturat);
        }

        // POST: Fakturats/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Fakturat fakturat = db.Fakturats.Find(id);
            db.Fakturats.Remove(fakturat);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
