﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HospitalDB;

namespace HospitalSystem.Web.Controllers
{
    public class PacientetsController : Controller
    {
        private HospitalSystemEntities1 db = new HospitalSystemEntities1();

        // GET: Pacientets
        public ActionResult Index()
        {
            var pacientets = db.Pacientets.Include(p => p.Gender).Include(p => p.Doctor);
            return View(pacientets.ToList());
        }

        // GET: Pacientets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pacientet pacientet = db.Pacientets.Find(id);
            if (pacientet == null)
            {
                return HttpNotFound();
            }
            return View(pacientet);
        }

        // GET: Pacientets/Create
        public ActionResult Create()
        {
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Sex");
            ViewBag.DoctorID = new SelectList(db.Doctors, "Id", "Name");
            return View();
        }

        // POST: Pacientets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Surname,GroupBlood,DateOfBirth,GenderId,DoctorID")] Pacientet pacientet)
        {
            if (ModelState.IsValid)
            {
                db.Pacientets.Add(pacientet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Sex", pacientet.GenderId);
            ViewBag.DoctorID = new SelectList(db.Doctors, "Id", "Name", pacientet.DoctorID);
            return View(pacientet);
        }

        // GET: Pacientets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pacientet pacientet = db.Pacientets.Find(id);
            if (pacientet == null)
            {
                return HttpNotFound();
            }
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Sex", pacientet.GenderId);
            ViewBag.DoctorID = new SelectList(db.Doctors, "Id", "Name", pacientet.DoctorID);
            return View(pacientet);
        }

        // POST: Pacientets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Surname,GroupBlood,DateOfBirth,GenderId,DoctorID")] Pacientet pacientet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pacientet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Sex", pacientet.GenderId);
            ViewBag.DoctorID = new SelectList(db.Doctors, "Id", "Name", pacientet.DoctorID);
            return View(pacientet);
        }

        // GET: Pacientets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pacientet pacientet = db.Pacientets.Find(id);
            if (pacientet == null)
            {
                return HttpNotFound();
            }
            return View(pacientet);
        }

        // POST: Pacientets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pacientet pacientet = db.Pacientets.Find(id);
            db.Pacientets.Remove(pacientet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
