﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HospitalSystem.Web.Startup))]
namespace HospitalSystem.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
