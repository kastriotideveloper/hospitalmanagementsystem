﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Modelet
{
    public class Preciption
    {
        public int Id { get; set; }
        public string Medicine { get; set; }
        public int NoOfDays { get; set; }
        public string TakeIt { get; set; }
        public bool BeforeMeal { get; set; }
    }
}
