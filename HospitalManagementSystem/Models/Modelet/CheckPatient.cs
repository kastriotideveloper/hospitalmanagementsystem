﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Modelet
{
    public class CheckPatient
    {
        public int Id { get; set; }
        public string Symptoms { get; set; }
        public string Diagnosis { get; set; }
        public DateTime checkUpDate { get; set; }
        public string ExtraNote { get; set; }
    }
}
