﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Modelet
{
    public class Medication
    {
        public int Id { get; set; }
        public string NameOfMedication { get; set; }
    }
}
