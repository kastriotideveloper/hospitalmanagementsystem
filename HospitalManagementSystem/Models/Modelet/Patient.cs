﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Modelet
{
    public class Patient
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastNAme { get; set; }
        public string phoneNr { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }

    }
}
