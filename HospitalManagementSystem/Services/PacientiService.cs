﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HospitalDB;

namespace Services
{
    public class PacientiService : IPacienti
    {
        private HospitalSystemEntities1 db = new HospitalSystemEntities1();

        public void Delete(int? id)
        {
            var pacienti = db.Pacientets.Find(id);
            db.Pacientets.Remove(pacienti);
            db.SaveChanges();
        }

        public List<Pacientet> getPacientet()
        {
            return  db.Pacientets.ToList();
        }

        public Pacientet GetPacientetId(int? id)
        {
            return db.Pacientets.Find(id);
        }

        public void savePacientet(Pacientet pacientet)
        {
            db.Entry(pacientet).State = System.Data.Entity.EntityState.Unchanged;
            db.Pacientets.Add(pacientet);
            db.SaveChanges();
        }

        public void UpdatePacientet(Pacientet pacientet)
        {
            db.Entry(pacientet).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }
    }
}
