﻿using HospitalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IPacienti
    {
        List<Pacientet> getPacientet();
        void savePacientet(Pacientet pacientet);
        Pacientet GetPacientetId(int? id);
        void UpdatePacientet(Pacientet pacientet);
        void Delete(int? id);
    }
}
