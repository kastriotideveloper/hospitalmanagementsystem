﻿using HospitalDB;
using HospitalDB.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IDoctor
    {
        List<Doctor> getDoctors();
        void saveDoctor(Doctor doctors);
        Doctor GetDoctorId(int? id);
        void UpdateDoctor(Doctor doctors);
        void Delete(int? id);
         List<KontrollaViewModel> getDoktorin();
    }
}
