﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HospitalDB;
using HospitalDB.ViewModel;

namespace Services
{
    public class DotorService : IDoctor
    {
        private HospitalSystemEntities1 db = new HospitalSystemEntities1();
        public void Delete(int? id)
        {
            var doctor = db.Doctors.Find(id);
            db.Doctors.Remove(doctor);
            db.SaveChanges();
        }

        public List<Doctor> getDoctors()
        {
            return db.Doctors.ToList();
        }

        public List<KontrollaViewModel> getDoktorin()
        {
            var dok = db.Kontrollas.Select(x => new KontrollaViewModel()
            {
               Id = x.Id,
               Symptom = x.Symptom,
               Diagnostik = x.Diagnostik,
               DoctorId = x.DoctorId,
               Doctor = new DoctorViewModel()
               {
                   Id = x.Doctor.Id,
                   Address = x.Doctor.Address,
                   Email = x.Doctor.Email,
                   Kontrollas = x.Doctor.Kontrollas,
                   Name = x.Doctor.Name,
                   PhoneNr = x.Doctor.PhoneNr,
                   Profesion = x.Doctor.Profesion,
                   Surname = x.Doctor.Surname
               }

            }).ToList();
            return dok;
        }

        public Doctor GetDoctorId(int? id)
        {
            return db.Doctors.Find(id);
        }

        public void saveDoctor(Doctor doctors)
        {
            db.Entry(doctors).State = System.Data.Entity.EntityState.Unchanged;
            db.Doctors.Add(doctors);
            db.SaveChanges();
        }

        public void UpdateDoctor(Doctor doctors)
        {
            db.Entry(doctors).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }
    }
}
