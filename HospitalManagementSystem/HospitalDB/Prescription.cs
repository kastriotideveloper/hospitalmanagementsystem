//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HospitalDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Prescription
    {
        public int Id { get; set; }
        public string NumOfDays { get; set; }
        public string TakeIt { get; set; }
        public Nullable<byte> BeforeMeal { get; set; }
        public Nullable<int> MedicineId { get; set; }
        public Nullable<int> PaientId { get; set; }
    
        public virtual Pacientet Pacientet { get; set; }
    }
}
