﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalDB.ViewModel
{
    public class KontrollaViewModel
    {
        public int Id { get; set; }
        public string Symptom { get; set; }
        public string Diagnostik { get; set; }
        public int? DoctorId { get; set; }

        public  DoctorViewModel Doctor { get; set; }
    }
}
