﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalDB.ViewModel
{
    public class DoctorViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string PhoneNr { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Profesion { get; set; }
        public virtual ICollection<Kontrolla> Kontrollas { get; set; }
    }
}
